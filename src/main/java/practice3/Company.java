package practice3;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;


public class Company {
    private List<Employee> employees;

    public Company() {
        employees = new ArrayList<Employee>();
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);

    }

    public void startWork() {
        employees.forEach(new Consumer<Employee>() {
            public void accept(Employee e) {
                e.work();
            }
        });
    }
}

