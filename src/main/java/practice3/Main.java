package practice3;

public class Main {
    public static void main(String[] args) {
 Company company = new Company();
 company.addEmployee( new WebDeveloper("Azhar"));
 company.addEmployee(new JavaDeveloper("Zhanar"));
 company.addEmployee(new FullStackDeveloper("Marsel"));
 company.startWork();
    }
}
