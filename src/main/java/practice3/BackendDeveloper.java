package practice3;

public interface BackendDeveloper extends Developer {
    void writeBack();
}
