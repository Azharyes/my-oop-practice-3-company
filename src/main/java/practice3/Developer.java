package practice3;

public interface Developer  extends Person{
    void develop();
}
