package practice3;

public class FullStackDeveloper extends Worker implements FrontendDeveloper,BackendDeveloper {
public FullStackDeveloper(String name){
    super(name);
}

    public void writeBack() {
        System.out.println("My name is"+getName()+" I write Back in c#");
    }

    public void develop() {
writeBack();
writeFronted();
    }

    public void work() {
develop();
    }

    public void writeFronted() {
        System.out.println("My name is "+getName()+" I write Front in HTML");
    }
}
