package practice3;

public class JavaDeveloper extends Worker implements BackendDeveloper {
    public JavaDeveloper(String name) {
        super(name);
    }

    public void work() {
        develop();
    }

    public void writeBack() {
        System.out.println("My name is"+getName()+" I am using Java to write back");
    }

    public void develop() {
        writeBack();
    }


}
